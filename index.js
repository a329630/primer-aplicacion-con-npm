const log4js = require("log4js");

let logger = log4js.getLogger();

logger.level = "debug";

logger.info("La app inició correctamente");
logger.warn("Falta una librería en el sistema (Libreria x)");
logger.error("No se encontró la función x");
logger.fatal("No se pudo iniciar la app");

function sumar(x, y){
    return x + y;
}

module.exports = sumar;