const sumar = require("../index.js");
const assert = require("assert");
//Assert = afirmación

describe("Probar la suma de dos números", ()=>{
    //Afirmamos que 5 + 7 = 12
    it("5 + 7 = 12", ()=>{
        assert.equal(12, sumar(5, 7));
    });

    //Afirmamos que 5 + 7 != 57
    it("5 + 7 != 57", ()=>{
        assert.notEqual(57, sumar(5, 7));
    });
});